
stock void SetAbsVelocity(client, const float velocity[3])
{
	SetEntPropVector(client, Prop_Data, "m_vecAbsVelocity", velocity);
}


/**
 * Returns the greater of two float values.
 *
 * @param value1		First value.
 * @param value2		Second value.
 * @return				Greatest value.
 */
stock float FloatMax(float value1, float value2)
{
	if (value1 >= value2)
	{
		return value1;
	}
	return value2;
}

/**
 * Retrieves the name of an entity.
 *
 * @param entity			Index of the entity.
 * @param buffer			Buffer to store the name.
 * @param maxlength			Maximum length of the buffer.
 * @return					Number of non-null bytes written.
 */
stock int GetEntityName(int entity, char[] buffer, int maxlength)
{
	return GetEntPropString(entity, Prop_Data, "m_iName", buffer, maxlength);
}

/**
 * Retrieves the absolute origin of an entity.
 *
 * @param entity			Index of the entity.
 * @param result			Entity's origin if successful.
 * @return					Returns true if successful.
 */
stock bool GetEntityAbsOrigin(int entity, float result[3])
{
	if (!IsValidEntity(entity))
	{
		return false;
	}
	
	if (!HasEntProp(entity, Prop_Data, "m_vecAbsOrigin"))
	{
		return false;
	}
	
	GetEntPropVector(entity, Prop_Data, "m_vecAbsOrigin", result);
	return true;
}
/**
 * Rotate a vector on an axis.
 *
 * @param vec				Vector to rotate.
 * @param axis				Axis to rotate around.
 * @param theta				Angle in radians.
 * @param result			Rotated vector.
 */

stock void RotateVectorAxis(float vec[3], float axis[3], float theta, float result[3])
{
	float cosTheta = Cosine(theta);
	float sinTheta = Sine(theta);
	
	float axisVecCross[3];
	GetVectorCrossProduct(axis, vec, axisVecCross);
	
	for (int i = 0; i < 3; i++)
	{
		result[i] = (vec[i] * cosTheta) + (axisVecCross[i] * sinTheta) + (axis[i] * GetVectorDotProduct(axis, vec)) * (1.0 - cosTheta);
	}
}

/**
 * Rotate a vector by pitch and yaw.
 *
 * @param vec				Vector to rotate.
 * @param pitch				Pitch angle (in degrees).
 * @param yaw				Yaw angle (in degrees).
 * @param result			Rotated vector.
 */
stock void RotateVectorPitchYaw(float vec[3], float pitch, float yaw, float result[3])
{
	if (pitch != 0.0)
	{
		RotateVectorAxis(vec, view_as<float>({0.0, 1.0, 0.0}), DegToRad(pitch), result);
	}
	if (yaw != 0.0)
	{
		RotateVectorAxis(result, view_as<float>({0.0, 0.0, 1.0}), DegToRad(yaw), result);
	}
}
/**
 * Finds an entity by name or by name and classname.
 * Taken from smlib https://github.com/bcserv/smlib
 * This can take anywhere from ~0.2% to ~11% of frametime (i5-7600k) in the worst case scenario where
 * every entity which has a name (4096 of them) is iterated over. Your mileage may vary.
 *
 * @param name				Name of the entity to find.
 * @param className			Optional classname to match along with name.
 * @param ignorePlayers		Ignore player entities.
 * @return					Entity index if successful, INVALID_ENT_REFERENCE if not.
 */

stock int GOKZFindEntityByName(const char[] name, const char[] className = "", bool ignorePlayers = false)
{
	int result = INVALID_ENT_REFERENCE;
	if (className[0] == '\0')
	{
		// HACK: Double the limit to get non-networked entities too.
		// https://developer.valvesoftware.com/wiki/Entity_limit
		int realMaxEntities = GetMaxEntities() * 2;
		int startEntity = 1;
		if (ignorePlayers)
		{
			startEntity = MaxClients + 1;
		}
		for (int entity = startEntity; entity < realMaxEntities; entity++)
		{
			if (!IsValidEntity(entity))
			{
				continue;
			}
			
			char entName[65];
			GetEntityName(entity, entName, sizeof(entName));
			if (StrEqual(entName, name))
			{
				result = entity;
				break;
			}
		}
	}
	else
	{
		int entity = INVALID_ENT_REFERENCE;
		while ((entity = FindEntityByClassname(entity, className)) != INVALID_ENT_REFERENCE)
		{
			char entName[65];
			GetEntityName(entity, entName, sizeof(entName));
			if (StrEqual(entName, name))
			{
				result = entity;
				break;
			}
		}
	}
	return result;
}

/**
 * Gets entity index from the address to an entity.
 *
 * @param pEntity			Entity address.
 * @return					Entity index.
 * @error					Couldn't find offset for m_angRotation, m_vecViewOffset, couldn't confirm offset of m_RefEHandle.
 */
stock int GetEntityFromAddress(Address pEntity)
{
	static int offs_RefEHandle;
	if (offs_RefEHandle)
	{
		return EntRefToEntIndex(LoadFromAddress(pEntity + view_as<Address>(offs_RefEHandle), NumberType_Int32) | (1 << 31));
	}

	// if we don't have it already, attempt to lookup offset based on SDK information
	// CWorld is derived from CBaseEntity so it should have both offsets
	int offs_angRotation = FindDataMapInfo(0, "m_angRotation"), offs_vecViewOffset = FindDataMapInfo(0, "m_vecViewOffset");
	if (offs_angRotation == -1)
	{
		SetFailState("Could not find offset for ((CBaseEntity) CWorld)::m_angRotation");
	}
	else if (offs_vecViewOffset == -1)
	{
		SetFailState("Could not find offset for ((CBaseEntity) CWorld)::m_vecViewOffset");
	}
	else if ((offs_angRotation + 0x0C) != (offs_vecViewOffset - 0x04))
	{
		char game[32];
		GetGameFolderName(game, sizeof(game));
		SetFailState("Could not confirm offset of CBaseEntity::m_RefEHandle (incorrect assumption for game '%s'?)", game);
	}

	// offset seems right, cache it for the next call
	offs_RefEHandle = offs_angRotation + 0x0C;
	return GetEntityFromAddress(pEntity);
}

/**
 * Gets client index from CGameMovement class.
 *
 * @param addr							Address of CGameMovement class.
 * @param offsetCGameMovement_player	Offset of CGameMovement::player.
 * @return								Client index.
 * @error								Couldn't find offset for m_angRotation, m_vecViewOffset, couldn't confirm offset of m_RefEHandle.
 */
stock int GetClientFromGameMovementAddress(Address addr, int offsetCGameMovement_player)
{
	Address playerAddr = view_as<Address>(LoadFromAddress(view_as<Address>(view_as<int>(addr) + offsetCGameMovement_player), NumberType_Int32));
	return GetEntityFromAddress(playerAddr);
}
