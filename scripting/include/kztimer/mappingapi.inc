
#define GOKZ_MAPPING_API_VERSION_NONE 0 // the map doesn't have a mapping api version
#define GOKZ_MAPPING_API_VERSION 1

#define GOKZ_ANTI_BHOP_TRIGGER_DEFAULT_DELAY 0.2
#define GOKZ_TELEPORT_TRIGGER_DEFAULT_TYPE TeleportType_Normal
#define GOKZ_TELEPORT_TRIGGER_DEFAULT_DELAY 0.0
#define GOKZ_TELEPORT_TRIGGER_DEFAULT_USE_DEST_ANGLES true
#define GOKZ_TELEPORT_TRIGGER_DEFAULT_RESET_SPEED true
#define GOKZ_TELEPORT_TRIGGER_DEFAULT_RELATIVE_DESTINATION false
#define GOKZ_TELEPORT_TRIGGER_DEFAULT_REORIENT_PLAYER false
#define GOKZ_TELEPORT_TRIGGER_BHOP_MIN_DELAY 0.08

#define GOKZ_BHOP_NO_CHECKPOINT_TIME 0.15
#define GOKZ_MULT_NO_CHECKPOINT_TIME 0.11
#define GOKZ_LADDER_NO_CHECKPOINT_TIME 1.5

#define GOKZ_START_BUTTON_NAME "climb_startbutton"
#define GOKZ_END_BUTTON_NAME "climb_endbutton"
#define GOKZ_BONUS_START_BUTTON_NAME_REGEX "^climb_bonus(\\d+)_startbutton$"
#define GOKZ_BONUS_END_BUTTON_NAME_REGEX "^climb_bonus(\\d+)_endbutton$"
#define GOKZ_ANTI_BHOP_TRIGGER_NAME "climb_anti_bhop"
#define GOKZ_ANTI_CP_TRIGGER_NAME "climb_anti_checkpoint"
#define GOKZ_ANTI_PAUSE_TRIGGER_NAME "climb_anti_pause"
#define GOKZ_ANTI_JUMPSTAT_TRIGGER_NAME "climb_anti_jumpstat"
#define GOKZ_BHOP_RESET_TRIGGER_NAME "climb_bhop_reset"
#define GOKZ_TELEPORT_TRIGGER_NAME "climb_teleport"

#define GOKZ_BSP_HEADER_IDENTIFIER (('P' << 24) | ('S' << 16) | ('B' << 8) | 'V')
#define GOKZ_ENTLUMP_MAX_KEY 32
#define GOKZ_ENTLUMP_MAX_VALUE 1024

#define GOKZ_MAX_MAPTRIGGERS_ERROR_LENGTH 256

enum EntlumpTokenType
{
	EntlumpTokenType_OpenBrace,  // {
	EntlumpTokenType_CloseBrace, // }
	EntlumpTokenType_Identifier, // everything that's inside quotations
	EntlumpTokenType_Unknown,
	EntlumpTokenType_EndOfStream
};

// NOTE: corresponds to climb_teleport_type in kz_mapping_api.fgd
enum TeleportType
{
	TeleportType_Invalid = -1,
	TeleportType_Normal,
	TeleportType_MultiBhop,
	TeleportType_SingleBhop,
	TeleportType_SequentialBhop,
	TELEPORTTYPE_COUNT
};

enum TriggerType
{
	TriggerType_Invalid = 0,
	TriggerType_Teleport,
	TriggerType_Antibhop
};



enum struct EntlumpToken
{
	EntlumpTokenType type;
	char string[GOKZ_ENTLUMP_MAX_VALUE];
}

enum struct AntiBhopTrigger
{
	int entRef;
	int hammerID;
	float time;
}

enum struct TeleportTrigger
{
	int hammerID;
	TeleportType type;
	float delay;
	char tpDestination[256];
	bool useDestAngles;
	bool resetSpeed;
	bool relativeDestination;
	bool reorientPlayer;
}

enum struct TouchedTrigger
{
	TriggerType triggerType;
	int entRef; // entref of one of the TeleportTriggers
	int startTouchTick; // tick where the player touched the trigger
	int groundTouchTick; // tick where the player touched the ground
}
