
public Action:Client_JumpstatsTop(client, args)
{
	if (!IsValidClient(client))
	{
		return Plugin_Handled;
	}

	else if (!gB_KZTimerAPI)
	{
		PrintToChat(client, "Server is not using API");
		return Plugin_Handled;
	}

	else
	{
		KZTimerAPI_PrintGlobalJumpstatsTopMenu(client);
	}
	return Plugin_Handled;
}

public Action:Client_APIGlobalTop(client, args)
{
	if (!IsValidClient(client))
	{
		return Plugin_Handled;
	}

	else if (!gB_KZTimerAPI)
	{
		PrintToChat(client, "Server is not using API");
		return Plugin_Handled;
	}

	else if (args <= 0)
	{
		KZTimerAPI_PrintGlobalRecordTopMenu(client);
	}
	
	else if (args == 1)
	{
		char mapName[128];
		GetCmdArg(1, mapName, sizeof(mapName));

		KZTimerAPI_PrintGlobalRecordTop(client, mapName);
	}
	
	else if (args == 2)
	{
		char mapName[128];
		char runType[30];
		GetCmdArg(1, mapName, sizeof(mapName));
		GetCmdArg(2, runType, sizeof(runType));

		KZTimerAPI_PrintGlobalRecordTop(client, mapName, runType);
	}
	
	else if (args >= 3)
	{
		char mapName[128];
		char runType[30];
		char tickRate[30];
		GetCmdArg(1, mapName, sizeof(mapName));
		GetCmdArg(2, runType, sizeof(runType));
		GetCmdArg(3, tickRate, sizeof(tickRate));

		KZTimerAPI_PrintGlobalRecordTop(client, mapName, runType, StringToInt(tickRate));
	}
	return Plugin_Handled;
}

public Action:Client_GlobalCheck(client, args)
{
	if (gB_KZTimerAPI)
	{
		KZTimerAPI_GlobalCheck(client);
	}
	
	GlobalStatus status = GlobalsEnabled();
	if (status == GLOBALSTATUS_DISABLED_SELF_BUILT_BUTTONS)
	{
		PrintToChat(client, "[%cKZ%c] %cGlobal Records disabled. Reason: Self-built climb buttons detected. (only built-in buttons supported)",MOSSGREEN,WHITE,RED);
		return Plugin_Handled;
	}
	else if (status == GLOBALSTATUS_DISABLED_ENFORCER_DISABLED)
	{
		PrintToChat(client, "[%cKZ%c] %cGlobal Records disabled. Reason: Server settings enforcer disabled.",MOSSGREEN,WHITE,RED);
		return Plugin_Handled;
	}
	else if (status == GLOBALSTATUS_DISABLED_DOUBLEDUCKS_ENABLED)
	{
		PrintToChat(client, "[%cKZ%c] %cGlobal Records disabled. Reason: kz_double_duck is set to 1.",MOSSGREEN,WHITE,RED);
		return Plugin_Handled;
	}
	else if (status == GLOBALSTATUS_DISABLED_MOMSURFFIX_UNAVAILABLE)
	{
		PrintToChat(client, "[%cKZ%c] %cGlobal Records disabled. Reason: kztimer-momsurffix is not available.",MOSSGREEN,WHITE,RED);
		return Plugin_Handled;
	}
	else if (status == GLOBALSTATUS_DISABLED_KZTIMERAPI_UNAVAILABLE)
	{
		PrintToChat(client, "[%cKZ%c] %cGlobal Records disabled. Reason: kztimer-api is not available.",MOSSGREEN,WHITE,RED);
		return Plugin_Handled;
	}
	else if (status == GLOBALSTATUS_ENABLED)
	{
		PrintToChat(client, "[%cKZ%c] %cGlobal records are enabled.",MOSSGREEN,WHITE,GREEN);
	}
	
	return Plugin_Handled;
}

GlobalStatus GlobalsEnabled()
{
	if (!gB_KZTimerAPI)
	{
		return GLOBALSTATUS_DISABLED_KZTIMERAPI_UNAVAILABLE;
	}
	else if (g_global_SelfBuiltButtons)
	{
		return GLOBALSTATUS_DISABLED_SELF_BUILT_BUTTONS;
	}
	else if (!g_bEnforcer)
	{
		return GLOBALSTATUS_DISABLED_ENFORCER_DISABLED;
	}
	else if (g_iDoubleDuckCvar == 1)
	{
		return GLOBALSTATUS_DISABLED_DOUBLEDUCKS_ENABLED;
	}
	else if (!g_bMomsurffixAvailable)
	{
		return GLOBALSTATUS_DISABLED_MOMSURFFIX_UNAVAILABLE;
	}
	else
	{
		return GLOBALSTATUS_ENABLED;
	}
}
